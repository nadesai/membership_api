import json
from typing import List  # NOQA: F401

import stripe
import vcr
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import STRIPE_SECRET_KEY
from membership.database.base import Session, engine, metadata
from membership.database.models import (
    Identity,
    IdentityProviders,
    Member,
    PhoneNumber,
    Role,
)
from membership.web.base_app import app
from membership.web.payments import ErrorCodes, payments_api
from tests.flask_utils import post_json

payments_vcr = vcr.VCR(
    serializer='json',
    path_transformer=vcr.VCR.ensure_suffix('.json'),
    cassette_library_dir='tests/cassettes',
)
stripe.api_key = STRIPE_SECRET_KEY
app.register_blueprint(payments_api)


PAYMENT_METHODS = {
    'good_visa': 'pm_card_visa',
    'good_visa-debit': 'pm_card_visa_debit',
    'good_mc': 'pm_card_mastercard',
    'good_mc-debit': 'pm_card_mastercard_debit',
    'good_mc-prepaid': 'pm_card_mastercard_prepaid',
    'good_amex': 'pm_card_amex',
    'good_discover': 'pm_card_discover',
    'good_diners': 'pm_card_diners',
    'good_jcb': 'pm_card_jcb',
    'good_unionpay': 'pm_card_unionpay',
    'verify_3ds2-required': 'pm_card_threeDSecure2Required',
    'verify_3ds-required': 'pm_card_threeDSecureRequired',
    'declined_3ds-required': 'pm_card_threeDSecureRequiredChargeDeclined',
    'declined_address-failed': 'pm_card_avsLine1Fail',
    'declined_zip-failed': 'pm_card_avsZipFail',
    'declined_cvc-check-fail': 'pm_card_cvcCheckFail',
    'declined_customer-charge-fails': 'pm_card_chargeCustomerFail',
    'declined_generic': 'pm_card_chargeDeclined',
    'declined_insufficient-funds': 'pm_card_chargeDeclinedInsufficientFunds',
    'declined_fraudulent': 'pm_card_chargeDeclinedFraudulent',
    'declined_cvc': 'pm_card_chargeDeclinedIncorrectCvc',
    'declined_expired': 'pm_card_chargeDeclinedExpiredCard',
    'error_processing': 'pm_card_chargeDeclinedProcessingError',
    'error_3ds-required': 'pm_card_threeDSecureRequiredProcessingError',
}


def _build_payment_payload(
    *,
    email: str,
    phone_number: str = '408-123-2345',
    recurrence: str,
    can_pay: bool = True,
    payment_method: str = PAYMENT_METHODS['good_visa'],
):
    return {
        'email_address': email,
        'phone_number': phone_number,
        'first_name': 'Testy',
        'last_name': 'McTestFace',
        'pronouns': 'they/them',
        'address': '123 Race St',
        'city': 'San Jose',
        'zip_code': '95112',
        **(
            {
                'payment': {
                    'total_amount_cents': 6900,
                    'local_amount_cents': 4900,
                    'national_amount_cents': 2000,
                    'recurrence': recurrence,
                    'stripe_payment_method_id': payment_method,
                }
            }
            if can_pay
            else {}
        ),
    }


def _check_member(member: Member, payload):
    assert member.email_address == payload['email_address']
    assert member.first_name == payload['first_name']
    assert member.last_name == payload['last_name']
    assert member.pronouns == payload['pronouns']
    assert member.address == payload['address']
    assert member.city == payload['city']
    assert member.zipcode == payload['zip_code']


class TestWebPayments:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

    def teardown(self):
        metadata.drop_all(engine)

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_full_annually(self):
        email_address = 'test-stripe-full-annually@example.com'
        payload = _build_payment_payload(
            email=email_address,
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['good_visa'],
        )

        expected_response = {
            'email_address': email_address,
            'subscription': {
                'id': 'sub_GvhDhSINzO7isL',  # generated from Stripe API
                'amount': 6900,
                'status': 'active',
                'items': {
                    'local': {'amount': 4900, 'interval': 'year'},
                    'national': {'amount': 2000, 'interval': 'year'},
                },
                'cancel_at_period_end': False,
            },
        }

        raw_response = post_json(self.app, '/payment/dues', payload=payload)
        actual_response = json.loads(raw_response.data)

        assert expected_response == actual_response
        assert raw_response.status_code == 201

        session = Session()

        member = session.query(Member).one()
        _check_member(member, payload)

        phone_number = session.query(PhoneNumber).one()
        assert phone_number.number == f"+1 {payload['phone_number']}"
        assert phone_number.member_id == member.id

        subscription_identity = session.query(Identity).one()
        assert subscription_identity.provider_name == IdentityProviders.COLLECTIVE_DUES
        assert (
            subscription_identity.provider_id == actual_response['subscription']['id']
        )
        assert subscription_identity.member_id == member.id

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_full_monthly(self):
        email_address = 'test-stripe-full-monthly@example.com'
        payload = _build_payment_payload(
            email=email_address,
            recurrence='monthly',
            can_pay=True,
            payment_method=PAYMENT_METHODS['good_visa'],
        )

        expected_response = {
            'email_address': email_address,
            'subscription': {
                'id': 'sub_GvhDrTmZvNF4P2',  # generated from Stripe API
                'amount': 6900,
                'status': 'active',
                'items': {
                    'local': {'amount': 4900, 'interval': 'month'},
                    'national': {'amount': 2000, 'interval': 'month'},
                },
                'cancel_at_period_end': False,
            },
        }

        raw_response = post_json(self.app, '/payment/dues', payload=payload)
        actual_response = json.loads(raw_response.data)

        assert expected_response == actual_response
        assert raw_response.status_code == 201

        session = Session()

        member = session.query(Member).one()
        _check_member(member, payload)

        phone_number = session.query(PhoneNumber).one()
        assert phone_number.number == f"+1 {payload['phone_number']}"
        assert phone_number.member_id == member.id

        subscription_identity = session.query(Identity).one()
        assert subscription_identity.provider_name == IdentityProviders.COLLECTIVE_DUES
        assert (
            subscription_identity.provider_id == actual_response['subscription']['id']
        )
        assert subscription_identity.member_id == member.id

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_full_onetime(self):
        email_address = 'test-stripe-full-onetime@example.com'
        payload = _build_payment_payload(
            email=email_address,
            recurrence='one-time',
            can_pay=True,
            payment_method=PAYMENT_METHODS['good_visa'],
        )

        expected_response = {
            'email_address': email_address,
            'subscription': {
                'id': 'sub_GvhDfJxdXKuQXu',  # generated from Stripe API
                'amount': 6900,
                'status': 'active',
                'items': {
                    'local': {'amount': 4900, 'interval': 'year'},
                    'national': {'amount': 2000, 'interval': 'year'},
                },
                'cancel_at_period_end': True,
            },
        }

        raw_response = post_json(self.app, '/payment/dues', payload=payload)
        actual_response = json.loads(raw_response.data)

        assert expected_response == actual_response
        assert raw_response.status_code == 201

        session = Session()

        member = session.query(Member).one()
        _check_member(member, payload)

        phone_number = session.query(PhoneNumber).one()
        assert phone_number.number == f"+1 {payload['phone_number']}"
        assert phone_number.member_id == member.id

        subscription_identity = session.query(Identity).one()
        assert subscription_identity.provider_name == IdentityProviders.COLLECTIVE_DUES
        assert (
            subscription_identity.provider_id == actual_response['subscription']['id']
        )
        assert subscription_identity.member_id == member.id

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_customer_exists(self):
        email_address = 'test-stripe-customer-exists@example.com'
        payload = _build_payment_payload(
            email=email_address,
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['good_visa'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        # Desync the member DB from Stripe
        session = Session()
        identity = session.query(Identity).one()
        session.delete(identity)
        phone_number = session.query(PhoneNumber).one()
        session.delete(phone_number)
        role = session.query(Role).one()
        session.delete(role)
        member = session.query(Member).one()
        session.delete(member)
        session.commit()

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 409
        assert raw_response.json['err'] == ErrorCodes.CUSTOMER_EXISTS

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_invalid_phone_number(self):
        payload = _build_payment_payload(
            email='test-stripe-phone-number-bad@example.com',
            phone_number='abc-def-asdf',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['good_visa'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 400
        assert raw_response.json['err'] == ErrorCodes.PHONE_NUMBER_INVALID

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_member_exists(self):
        payload = _build_payment_payload(
            email='test-stripe-member-exists@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['good_visa'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)
        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 409
        assert raw_response.json['err'] == ErrorCodes.MEMBER_EMAIL_EXISTS

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_invalid_payment_method(self):
        payload = _build_payment_payload(
            email='test-stripe-payment-required@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method='abcdef',
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 400
        assert raw_response.json['err'] == ErrorCodes.PAYMENT_PROCESSOR_BAD_REQUEST

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_insufficient_funds(self):
        payload = _build_payment_payload(
            email='test-stripe-insufficient-funds@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_insufficient-funds'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            'Your card has insufficient funds.'
        )

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_fraudulent(self):
        payload = _build_payment_payload(
            email='test-stripe-fraudulent@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_fraudulent'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            'Your card was declined.'
        )

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_cvc(self):
        payload = _build_payment_payload(
            email='test-stripe-bad-cvc@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_cvc'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            "Your card's security code is incorrect."
        )

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_expired(self):
        payload = _build_payment_payload(
            email='test-stripe-expired@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_expired'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            'Your card has expired.'
        )

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_processing_error(self):
        payload = _build_payment_payload(
            email='test-stripe-processing@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['error_processing'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            'An error occurred while processing your card. Try again in a little bit.'
        )

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_declined_generic(self):
        payload = _build_payment_payload(
            email='test-stripe-declined-generic@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_generic'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            'Your card was declined.'
        )

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_zip_failed(self):
        payload = _build_payment_payload(
            email='test-stripe-declined-zip@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_zip-failed'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        # In the future perhaps turn on Radar to make this a 402?
        assert raw_response.status_code == 201

    @payments_vcr.use_cassette()
    def test_create_dues_subscription_charge_fails(self):
        payload = _build_payment_payload(
            email='test-stripe-charge-fails@example.com',
            phone_number='408-123-2345',
            recurrence='annually',
            can_pay=True,
            payment_method=PAYMENT_METHODS['declined_customer-charge-fails'],
        )

        raw_response = post_json(self.app, '/payment/dues', payload=payload)

        assert raw_response.status_code == 402
        assert raw_response.json['err'] == ErrorCodes.subscription_payment_failed_msg(
            'Your card was declined.'
        )


# NOTE: To write new tests for the payments system that use Stripe,
# you MUST put a valid Stripe test key in example.env.
# You can create a new personal account in test mode and use the
# key from that account to do this.
#
# When writing tests that deal with Stripe-generated IDs, you have
# to first talk to the Stripe API once to get the ID with VCR recording on,
# use the ID from the Stripe API in your test responses, and then use the
# cassette playback of the API interaction for subsequent test runs
