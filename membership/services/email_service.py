"""Operations related to email addresses."""

from typing import List, Optional, Sequence

from config import EMAIL_CONNECTOR
from membership.database.base import Session
from membership.database.models import AdditionalEmailAddress, Email, ForwardingAddress
from membership.database.statements import insert_or_ignore
from membership.models import AuthContext
from membership.util.email import get_email_connector

email_connector = get_email_connector(EMAIL_CONNECTOR)


class EmailExistsError(Exception):
    """An error raised when attempting to create an email address that already exists."""

    pass


class EmailService:
    """Operations related to email addresses."""

    def list(self, ctx: AuthContext) -> List[Email]:
        """Returns a list of email addresses."""
        emails: List[Email] = ctx.session.query(Email).all()
        return emails

    def create(
        self,
        ctx: AuthContext,
        email_address: str,
        forwarding_addresses: Sequence[str] = [],
    ) -> Email:
        """Creates a new email address."""
        if (
            ctx.session.query(Email)
            .filter_by(email_address=email_address)
            .one_or_none()
        ):
            raise EmailExistsError()
        email = Email()
        email.email_address = email_address
        ctx.session.add(email)
        for forwarding_email in forwarding_addresses:
            forwarding_address = ForwardingAddress()
            forwarding_address.incoming_email = email
            forwarding_address.forward_to = forwarding_email
            ctx.session.add(forwarding_address)
        email.external_id = email_connector.update_email(email)
        ctx.session.commit()
        return email

    def get(self, ctx: AuthContext, email_id: int) -> Optional[Email]:
        """Returns a specific email address."""
        return ctx.session.query(Email).get(email_id)

    def update(
        self,
        ctx: AuthContext,
        email_id: int,
        email_address: Optional[str] = None,
        forwarding_addresses: Optional[List[str]] = None,
    ) -> Optional[Email]:
        """Updates an email address."""
        email: Optional[Email] = ctx.session.query(Email).get(email_id)
        if email is None:
            return None
        if email_address is not None:
            email.email_address = email_address
        if forwarding_addresses is None:
            forwarding_addresses = []
        to_add = set(forwarding_addresses)
        for forwarding_address in list(email.forwarding_addresses):
            if forwarding_address.forward_to in to_add:
                # no need to alter existing email addresses
                to_add.remove(forwarding_address.forward_to)
            else:
                # remove email addresses not in the new set
                email.forwarding_addresses.remove(forwarding_address)
                ctx.session.delete(forwarding_address)
        # add new email addresses
        for forwarding_email in to_add:
            forwarding_address = ForwardingAddress()
            forwarding_address.incoming_email = email
            forwarding_address.forward_to = forwarding_email
            ctx.session.add(forwarding_address)
        email.external_id = email_connector.update_email(email)
        ctx.session.commit()
        return email

    def delete(self, ctx: AuthContext, email_id: int) -> bool:
        """Deletes an email address."""
        email: Optional[Email] = ctx.session.query(Email).get(email_id)
        if not email:
            return False
        external_id = email.external_id
        ctx.session.delete(email)
        email_connector.remove_email(external_id)
        ctx.session.commit()
        return True


class AdditionalEmailAddressService:
    def add_all(self, session: Session, addresses: List[AdditionalEmailAddress]):
        statement = insert_or_ignore(session, AdditionalEmailAddress)
        session.execute(
            statement,
            [
                {
                    'member_id': address.member_id,
                    'name': address.name,
                    'email_address': address.email_address,
                }
                for address in addresses
            ],
        )
        session.commit()
