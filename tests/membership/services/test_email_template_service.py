import pytest
from datetime import datetime, timezone
from jsonschema import ValidationError

from membership.database.base import engine, metadata, Session
from membership.models import Member
from membership.models.authz import AuthContext
from membership.services import EmailTemplateService


class TestEmailTemplateService:
    email_template_service = EmailTemplateService()
    example1 = {
        "name": "Example 1",
        "subject": "Subject 1",
        "body": "Body 1",
    }

    def setup_method(cls, method):
        metadata.create_all(engine)

    def teardown_method(cls, method):
        metadata.drop_all(engine)

    def test_upsert_name_required(self):
        session = Session()
        member = Member(id=1)
        ctx = AuthContext(member, session)
        with pytest.raises(ValidationError) as e:
            self.email_template_service.upsert(ctx, {})
        assert 'name' in str(e)

    def test_upsert_new(self):
        session = Session()
        member = Member(id=1)
        ctx = AuthContext(member, session)
        template1 = self.email_template_service.upsert(ctx, self.example1)
        assert template1.id is not None
        assert template1.last_updated <= datetime.now(timezone.utc)

    def test_upsert_existing(self):
        session = Session()
        member = Member(id=1)
        ctx = AuthContext(member, session)
        template1 = self.email_template_service.upsert(ctx, self.example1)
        first_update = template1.last_updated
        template2 = self.email_template_service.upsert(ctx, {"body": "updated"}, template1)
        assert template1.id == template2.id
        assert template2.body == 'updated'
        assert template2.last_updated > first_update
