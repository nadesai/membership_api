from datetime import datetime
from unittest.mock import patch

from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import Committee, Email, Member, NationalMembershipData, Role
from membership.models import AuthContext
from membership.services import CommitteeService

committee_service = CommitteeService()


class TestCommitteeService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role1 = Role(member=m, role='member', date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role='admin', date_created=datetime(2016, 1, 1))
        email = Email(external_id=1, email_address=SUPER_USER_EMAIL, committee_id=1)
        session.add_all([role1, role2, email])
        membership = NationalMembershipData(
                member=m,
                ak_id='73750',
                first_name='Huey',
                last_name='Newton',
                address_line_1='123 Main St',
                city='Oakland',
                country='United States',
                zipcode='94612',
                dues_paid_until=datetime(3020, 6, 1, 0),
              )
        session.add(membership)

        committee = Committee(id=1, name='Testing Committee')

        session.add(committee)

        session.commit()

        self.session = session
        self.member = m
        self.ctx = AuthContext(self.member, self.session)

    def teardown(self):
        metadata.drop_all(engine)


class TestCommitteeServiceList(TestCommitteeService):
    def test_list_returns_committee_info(self):
        assert committee_service.list(self.ctx) == [{
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
        }]


class TestCommitteeServiceCreate(TestCommitteeService):
    def test_create_creates_a_new_committee_and_returns_the__details(self):
        name = 'Committee on Mock Data'
        committee_details = committee_service.create(
            self.ctx,
            name=name,
            admins=[SUPER_USER_EMAIL],
        )

        assert committee_details == {
            'id': 2,
            'name': 'Committee on Mock Data',
            'email': None,
            'viewable': True,
            'provisional': False,
            'admins': [{'id': 1, 'name': ''}],
            'members': [],
            'active': []
        }

        committee = self.session.query(Committee).filter_by(name=name).one_or_none()
        assert committee is not None
        assert self.session.query(Role).filter_by(
            committee=committee,
            member_id=1,
            role='admin',
        ).count() == 1


class TestCommitteeServiceGet(TestCommitteeService):
    def test_get_returns_committee_info(self):
        assert committee_service.get(self.ctx, 1) == {
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
            'admins': [],
            'members': [],
            'active': []
        }

    def test_get_returns_none_for_missing_committee(self):
        assert committee_service.get(self.ctx, -1) is None


class TestCommitteeServiceRequestMembership(TestCommitteeService):
    def test_request_membership_returns_true_on_success(self):
        assert committee_service.request_membership(
            self.ctx,
            committee_id=1,
            member_name=self.ctx.requester.name,
            member_email=self.ctx.requester.email_address,
        )

    def test_request_membership_returns_none_when_committee_has_no_email(self):
        assert committee_service.request_membership(
            self.ctx,
            committee_id=-1,
            member_name=self.ctx.requester.name,
            member_email=self.ctx.requester.email_address,
        ) is None

    @patch(
        'membership.services.committee_service.send_committee_request_email',
        autospec=True,
        side_effect=Exception(),
    )
    def test_request_membership_returns_false_when_sending_email_raises_an_exception(self, _):
        assert not committee_service.request_membership(
            self.ctx,
            committee_id=1,
            member_name=self.ctx.requester.name,
            member_email=self.ctx.requester.email_address,
        )
