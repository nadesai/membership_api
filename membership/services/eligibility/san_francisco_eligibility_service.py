from typing import List, Tuple, Union, Optional
from datetime import datetime

from .eligibility_service import EligibilityService

from membership.database.base import Session
from membership.database.models import Attendee, Member, Meeting
from membership.models import MemberAsEligibleToVote, AttendeeAsEligibleToVote
from membership.services import AttendeeService
from membership.services.errors import ValidationError
from membership.repos import MeetingRepo
from membership.util.time import get_current_time


class SanFranciscoEligibilityService(EligibilityService):

    class ErrorCodes:
        MEETING_ENDED = "MEETING_ENDED"

    def __init__(self, meetings: MeetingRepo, attendee_service: AttendeeService) -> None:
        self.meetings = meetings
        self.attendee_service = attendee_service

    def members_as_eligible_to_vote(
        self,
        session: Session,
        members: List[Member],
        since: Optional[datetime] = None
    ) -> List[MemberAsEligibleToVote]:
        if since is None:
            since = get_current_time()

        recent_meetings = self.meetings.most_recent(session, 3, since)

        def member_as_eligible(member: Member) -> MemberAsEligibleToVote:
            (is_eligible, message) = self._eligibility_details(member, recent_meetings)
            return MemberAsEligibleToVote(
                member,
                is_eligible=is_eligible,
                message=message,
            )

        return [member_as_eligible(m) for m in members]

    def update_eligibility_to_vote_at_attendance(
        self,
        session: Session,
        member: Optional[Union[Member, int, str]] = None,
        meeting: Optional[Union[Meeting, int, str]] = None,
        attendee: Optional[Union[Attendee, int, str]] = None
    ) -> None:
        attendee: Attendee = self.attendee_service.retrieve_attendee(
            session,
            member,
            meeting,
            attendee
        )

        member: Member = attendee.member
        meeting: Meeting = attendee.meeting

        if meeting.end_time != None and meeting.end_time < get_current_time():
            raise ValidationError(
                SanFranciscoEligibilityService.ErrorCodes.MEETING_ENDED,
                'May not update eligibility to vote after meeting has ended.'
            )

        members_as_eligible_to_vote = self.members_as_eligible_to_vote(
            session, [member], meeting.start_time
        )

        if members_as_eligible_to_vote and members_as_eligible_to_vote[0].is_eligible:
            attendee.eligible_to_vote = True
        else:
            attendee.eligible_to_vote = False

        session.add(attendee)
        session.commit()

    @staticmethod
    def attendees_as_eligible_to_vote(meeting: Meeting) -> List[AttendeeAsEligibleToVote]:
        def attendee_as_eligible(attendee: Attendee) -> AttendeeAsEligibleToVote:
            return AttendeeAsEligibleToVote(
                attendee,
                message="{}eligible".format('' if bool(attendee.eligible_to_vote) else 'not '),
            )

        return [attendee_as_eligible(a) for a in meeting.attendees]

    @staticmethod
    def _eligibility_details(member: Member, recent_meetings: List[Meeting]) -> Tuple[bool, str]:
        """
        True if the member attended the last two out of three meetings or has an active role,
        False otherwise
        """
        attended_meeting_ids = {a.meeting_id for a in member.meetings_attended}
        recent_meeting_attendance = [
            m for m in recent_meetings if m.id in attended_meeting_ids
        ]
        has_attended = len(recent_meeting_attendance) >= 2

        is_active = has_attended or any(
            role.role == 'active' and role.committee is not None and not role.committee.provisional
            for role in member.roles
        )
        is_member = any(role.role == 'member' and role.committee_id is None
                        for role in member.roles)
        is_eligible = is_member and is_active

        message = 'eligible' if is_eligible else 'not eligible'
        if recent_meeting_attendance:
            message += ' ({})'.format(
                ', '.join(m.start_time.strftime('%b') for m in recent_meeting_attendance)
            )
        return (
            is_eligible,
            message,
        )
