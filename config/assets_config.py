from config.lib import from_env

ASSETS_ENGINE = from_env.get_str('ASSETS_ENGINE', 'filesystem')
ASSETS_PATH_PREFIX = from_env.get_str('ASSETS_PATH_PREFIX', 'assets')

if ASSETS_ENGINE == 's3':
    ASSETS_BUCKET = from_env.get_str('ASSETS_BUCKET', 'dsasf')
    ASSETS_BUCKET_REGION = from_env.get_str('ASSETS_BUCKET_REGION', 'us-west-1')
