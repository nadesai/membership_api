import logging
from typing import Dict, List, Optional, Set, cast  # NOQA F401

from flask import Blueprint, Response, request
from sqlalchemy.exc import IntegrityError

from config import EMAIL_CONNECTOR, EMAIL_TOPICS_ADD_MEMBER_TOKEN
from membership.database.base import Session
from membership.database.models import EmailTemplate  # NOQA F401
from membership.database.models import (  # NOQA F401
    InterestTopic,
    Meeting,
    Member,
    PhoneNumber,
)
from membership.models import AuthContext, NoAuthContext
from membership.repos import MeetingRepo
from membership.schemas.rest import EmailTopicsRest
from membership.services import (
    AttendeeService,
    EmailTemplateService,
    InterestTopicsService,
    MemberService,
    PhoneNumberService,
)
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.util.email import get_email_connector, parse_combined_sender
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound, Ok, Unauthorized, requires_json

logger = logging.getLogger(__name__)
email_topics_api = Blueprint("email_topics_api", __name__)
interest_topics_service = InterestTopicsService
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(
    meeting_repository, attendee_service
)
member_service = MemberService(eligibility_service)
email_template_service = EmailTemplateService
email_connector = get_email_connector(EMAIL_CONNECTOR)
phone_numbers = PhoneNumberService()


@email_topics_api.route("/email_topics", methods=["GET"])
@requires_auth("admin")
def list_email_topics(ctx: AuthContext) -> Response:
    return Ok(
        [
            EmailTopicsRest.format(topic)
            for topic in interest_topics_service.list_all_interest_topics(ctx)
        ]
    )


@email_topics_api.route("/email_topics/member_count/<topic_name>", methods=["GET"])
@requires_auth("admin")
def member_count(ctx: AuthContext, topic_name) -> Response:
    topic: Optional[
        InterestTopic
    ] = interest_topics_service.find_interest_topic_by_name(ctx, topic_name)
    if topic is None:
        return NotFound("Topic {} does not exist".format(topic_name))

    members = interest_topics_service.list_interest_topic_members(ctx, topic)
    return Ok(len(members))


@email_topics_api.route("/email_topics/add_member", methods=["POST"])
def add_member_to_topics() -> Response:
    logger.info("POST /email_topics/add_member")
    user_input = request.form

    email_address: str = user_input.get("email", "").strip()
    first_name: str = user_input.get("first_name", "").strip()
    last_name: str = user_input.get("last_name", "").strip()
    phone_number: str = user_input.get("phone_number", "").strip()
    token: str = user_input.get("token", "")
    committee_interests: List[str] = (
        user_input.get("topics") or user_input.get("committee_interests", "")
    ).split(",")
    volunteer_interests: List[str] = (
        user_input.get("volunteer_interests", "").split(",")
    )
    topics: Set[str] = {
        topic
        for topic in (t.strip() for t in (committee_interests + volunteer_interests))
        if topic != ""
    }

    if token != EMAIL_TOPICS_ADD_MEMBER_TOKEN:
        return Unauthorized("Missing or invalid token")

    if email_address is None or len(email_address) == 0:
        return BadRequest('Missing required parameter "email"')

    ctx = NoAuthContext(Session())

    member = member_service.find_by_email(email_address, ctx.session)
    if member is None:
        # TODO: Centralize member creation in MemberService (this code is duplicated in members.py)
        member = Member(
            first_name=first_name, last_name=last_name, email_address=email_address,
        )
        ctx.session.add(member)
        try:
            ctx.session.flush()
        except IntegrityError:
            ctx.session.rollback()
            return BadRequest(f"Failed to create member with address: {email_address}")

    pn: Optional[PhoneNumber] = phone_numbers.create_number(phone_number, "WebForm")
    if pn:
        phone_numbers.upsert(ctx.session, member.id, pn)

    if len(topics) > 0:
        logger.info(f"Saving member interests: {topics}")
        interest_topics_service.create_interests(ctx, member.id, topics)

    ctx.session.commit()
    return Ok()


@email_topics_api.route("/email_topics/send", methods=["POST"])
@requires_auth("admin")
@requires_json
def send_email_to_topic_members(ctx: AuthContext) -> Response:
    topic_str: Optional[str] = request.json.get("topic")
    topic_id_str: Optional[str] = request.json.get("topic_id")
    if not topic_id_str and not topic_str:
        return BadRequest('Missing parameter "topic_id"')
    either_topic_str = cast(str, topic_id_str or topic_str)
    topic_id: int = int(either_topic_str)

    template_id: int = request.json.get("template_id")
    if template_id is None:
        return BadRequest('Missing parameter "template_id"')

    # TODO: validate sending email address?
    sending_address: str = request.json.get("sending_address")
    if sending_address is None:
        return BadRequest('Missing parameter "sending_address"')

    combined_sender = parse_combined_sender(sending_address)
    if combined_sender is None:
        if not email_connector.is_valid_email(sending_address):
            return BadRequest(
                (
                    "Unable to parse sender."
                    'Provide a valid email or a sender in "Name <email@example.com>" format.'
                )
            )
        else:
            sender_name = "DSA"
            sender_email = sending_address
    else:
        sender_name, sender_email = combined_sender

    topic: InterestTopic = interest_topics_service.find_interest_topic(ctx, topic_id)
    if topic is None:
        return NotFound("Interest topic not found")

    template: EmailTemplate = ctx.session.query(EmailTemplate).get(template_id)
    if template is None:
        return NotFound("Email template not found")

    tag: str = f"{topic.name}_templates"

    members: List[Member] = interest_topics_service.list_interest_topic_members(
        ctx, topic
    )

    recipient_variables: Dict[str, Dict] = {member: {} for member in members}
    email_connector.send_member_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject=template.subject,
        email_template=template.body,
        recipient_variables=recipient_variables,
        tag=tag,
    )

    return Ok()
