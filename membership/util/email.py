import json
import logging
import re
from typing import Any, Dict, List, Optional, Tuple
from urllib.parse import urljoin

import pkg_resources
import pytz
import requests
from overrides import overrides

import membership
from config import (
    CHAPTER_ADDRESS,
    CHAPTER_BANNER_IMAGE_URL,
    CHAPTER_ID,
    CHAPTER_NAME,
    CHAPTER_SHORT_NAME,
    EMAIL_API_ID,
    EMAIL_API_KEY,
    EMAIL_DOMAIN,
    MAILGUN_URL,
    PORTAL_URL,
    SUPPORT_EMAIL_ADDRESS,
    USE_EMAIL,
)
from membership.database.models import (
    Email,
    Meeting,
    Member,
    ProxyToken,
    ProxyTokenState,
    MeetingInvitation)

ValidEmail = r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
ValidUSPhone = r"^\D*\d{3}\D*\d{3}\D*\d{4}\D*$"


class EmailConnector:
    def send_member_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '',
        add_unsubscribe_link: bool = True,
    ):
        pass

    def send_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[str, Dict[Any, str]],
        to_emails: List[str],
        tag: str = '',
    ):
        pass

    def update_email(self, email: Email) -> str:
        return "dummy_external_mail_id"

    def remove_email(self, external_id: str) -> None:
        pass

    def is_valid_email(self, address: Optional[str]) -> bool:
        if not address:
            return False
        found_match = re.search(ValidEmail, address)
        return True if found_match else False


class MailgunEmailConnector(EmailConnector):
    @overrides
    def send_member_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '',
        add_unsubscribe_link: bool = True,
    ):
        """
        Create an email campaign from the sender using the given MailGun email template formatted
        string and substitute the recipient variables for every email key in the dict.

        NOTE: If `recipient_variables` is empty, then the email is sent to nobody, otherwise the
        email is sent to every key of the dictionary given.

        :param sender: the from field of the email to be received
        :param subject: the subject line of the email
        :param email_template: the full template with all variables / recipient variables declared
        :param recipient_variables: all the recipients of this email by email address with
                                    all variables associated with this address in the values
        """
        filtered_recipient_variables = {
            member.email_address: obj
            for member, obj in recipient_variables.items()
            if not member.do_not_email
        }
        if add_unsubscribe_link:
            email_template = (
                f"{email_template}\n\n\n"
                "to unsubscribe click here: %tag_unsubscribe_url%"
            )
        self.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=email_template,
            recipient_variables=filtered_recipient_variables,
            to_email=list(filtered_recipient_variables.keys()),
        )

    @overrides
    def send_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[str, Dict[Any, str]],
        to_emails: List[str],
        tag: str = '',
    ):
        url = f'https://api.mailgun.net/v3/{EMAIL_DOMAIN}/messages'
        payload = [
            ('from', f'{sender_name} <{sender_email}>'),
            ('recipient-variables', json.dumps(recipient_variables)),
            ('subject', subject),
            ('html', email_template),
            ('o:tag', tag),
        ]
        payload.extend([("to", email) for email in to_emails])
        if USE_EMAIL:
            r = requests.post(url, data=payload, auth=("api", EMAIL_API_KEY))
            if r.status_code > 299:
                logging.error(r.text)

    @overrides
    def update_email(self, email: Email) -> str:
        """
        Create or update routes in Mailgun
        :param email: the incoming email address (should have a least one forwarding address)
        :return: the external_id from mailgun
        """
        payload = [
            ('priority', '0'),
            (
                'description',
                'Forwarding rule for {address}'.format(address=email.email_address),
            ),
            (
                'expression',
                'match_recipient("{address}")'.format(address=email.email_address),
            ),
        ]
        payload.extend(
            [
                ("action", 'forward("{address}")'.format(address=forward.forward_to))
                for forward in email.forwarding_addresses
            ]
        )
        if email.external_id:
            r = requests.put(
                MAILGUN_URL + '/' + email.external_id,
                data=payload,
                auth=('api', EMAIL_API_KEY),
            )
            if r.status_code > 299:
                raise Exception('Mailgun api failed to update email.')
            return str(email.external_id)
        else:
            r = requests.post(MAILGUN_URL, data=payload, auth=("api", EMAIL_API_KEY))
            if r.status_code > 299:
                raise Exception('Mailgun api failed to create email.')
            return str(r.json().get('route').get('id'))

    @overrides
    def remove_email(self, external_id: str):
        r = requests.delete(
            MAILGUN_URL + '/' + external_id, auth=('api', EMAIL_API_KEY)
        )
        if r.status_code > 299:
            raise Exception("Mailgun api failed to delete email.")


class MailjetEmailConnector(EmailConnector):
    @overrides
    def send_member_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '',
        add_unsubscribe_link: bool = True,
    ):
        filtered_recipient_variables = {
            member.email_address: obj
            for member, obj in recipient_variables.items()
            if not member.do_not_email
        }
        if add_unsubscribe_link:
            email_template = (
                f'{email_template}\n\n\n to unsubscribe click here: [[mj:unsub_link]]'
            )
        self.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=email_template,
            recipient_variables=filtered_recipient_variables,
            to_emails=list(filtered_recipient_variables.keys()),
        )

    def send_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[str, Dict[Any, str]],
        to_emails: List[str],
        tag: str = '',
    ):
        mailjet_url = f"https://api.mailjet.com/v3/send"
        payload = {
            'FromName': sender_name,
            'FromEmail': sender_email,
            'Subject': subject,
            'Mj-TemplateLanguage': True,
            'TemplateErrorReporting': SUPPORT_EMAIL_ADDRESS,
            'TemplateErrorDeliver': True,
            'Html-part': self._parse_existing_templates(email_template),
            'Recipients': [
                {'Email': email_address, 'Vars': recipient_variables[email_address]}
                for email_address in to_emails
            ],
        }
        if USE_EMAIL:
            r = requests.post(
                mailjet_url, json=payload, auth=(EMAIL_API_ID, EMAIL_API_KEY)
            )
            if r.status_code > 299:
                logging.error(r.text)

    def _parse_existing_templates(self, template: str) -> str:
        return re.sub(r"%recipient\.(.*?)%", r"[[var:\1]]", template)

    @overrides
    def update_email(self, email: Email) -> str:
        raise NotImplementedError("Mailjet does not support forwarding rules")

    @overrides
    def remove_email(self, external_id: str):
        raise NotImplementedError("Mailjet does not support forwarding rules")


def get_email_connector(connector_name: str):
    """Return an instance of the `EmailConnector` implementation specified by
    `config.EMAIL_CONNECTOR`."""

    if connector_name == "mailgun":
        return MailgunEmailConnector()
    elif connector_name == 'mailjet':
        return MailjetEmailConnector()
    elif connector_name == "noop":
        return EmailConnector()
    else:
        logging.warning(
            f'`config.EMAIL_CONNECTOR` is set to an unsupported value {connector_name}. '
            'Defaulting to "mailgun". Please configure the environment to set this value to'
            '"mailgun", "mailjet", or "noop". '
            'In the future, an unset value will be an error. '
        )
        return MailgunEmailConnector()


WELCOME_TEMPLATE_BY_ID: Dict[str, str] = {
    'san-francisco': 'templates/sf_welcome_email.html',
    'silicon-valley': 'templates/sv_welcome_email.html',
}

WELCOME_VERIFY_TEMPLATE_BY_ID: Dict[str, str] = {
    'san-francisco': 'templates/sf_welcome_email_verification.html'
}


def _get_welcome_template_for_id(*, chapter_id: str, verify_url: str) -> str:
    if verify_url is None:
        return WELCOME_TEMPLATE_BY_ID[chapter_id]
    else:
        return WELCOME_VERIFY_TEMPLATE_BY_ID[chapter_id]


def send_welcome_email(
    email_connector: EmailConnector, member: Member, verify_url: str,
):
    # send a welcome email with the email verification / password reset link from Auth0
    sender_name = CHAPTER_NAME
    sender_email = SUPPORT_EMAIL_ADDRESS

    filename = _get_welcome_template_for_id(
        chapter_id=CHAPTER_ID, verify_url=verify_url
    )
    template = (
        pkg_resources.resource_string(membership.__name__, filename)
        .decode("utf-8")
        .format(PORTAL_URL=PORTAL_URL)
    )

    recipient_variables = {
        member: {
            'name': member.first_name,
            'link': verify_url,
            'chapter_name': CHAPTER_NAME,
            'chapter_short_name': CHAPTER_SHORT_NAME,
        }
    }
    email_connector.send_member_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject=f'Welcome to the {CHAPTER_SHORT_NAME} Membership Portal, %recipient.name%!',
        email_template=template,
        recipient_variables=recipient_variables,
    )


def _generate_confirm_link(token: str) -> str:
    return urljoin(PORTAL_URL, f'/verify-email/{token}')


def send_meeting_confirmation_email(
    *,
    email_connector: EmailConnector,
    member: Member,
    meeting: Meeting,
    token: str,
    email_address_override: Optional[str] = None,
    greeting: str = "Great to meet you",
):
    sender_name = CHAPTER_NAME
    sender_email = SUPPORT_EMAIL_ADDRESS
    filename = 'templates/confirm_attendance.html'
    template = pkg_resources.resource_string(membership.__name__, filename).decode(
        'utf-8'
    )

    subject = f"Please confirm your attendance at {meeting.name}"

    recipient_variables = {
        'greeting': greeting,
        'name': member.first_name,
        'meeting_name': meeting.name,
        'meeting_date': meeting.start_time.astimezone(
            tz=pytz.timezone('US/Pacific')  # HACK TODO: Assume Pacific timezone for now
        ).strftime('%A, %B %d, %Y at %I:%M %p'),
        'chapter_name': CHAPTER_NAME,
        'chapter_address': CHAPTER_ADDRESS,
        'confirm_link': _generate_confirm_link(token),
        'support_email': SUPPORT_EMAIL_ADDRESS,
        'chapter_banner_image_url': CHAPTER_BANNER_IMAGE_URL,
    }

    if email_address_override is None:
        email_connector.send_member_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=template,
            recipient_variables={member: recipient_variables},
            add_unsubscribe_link=False,
        )
    else:
        email_connector.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=template,
            recipient_variables={email_address_override: recipient_variables},
            to_emails=[email_address_override],
        )


def send_committee_request_email(
    email_connector: EmailConnector, name: str, member_email: str, email: str,
):
    sender_name = 'DSA SF Tech Committee'
    sender_email = SUPPORT_EMAIL_ADDRESS

    # Something weird is happening where the field is
    # not being populated via the recipient variables on prod only.
    # To deal with this the template is inlined here.
    template = f"""
    <html>
    <body>
    <p>Dear co-chairs,</p>
    <p>A member, {name}, is requesting to be added to your list of active members.</p>
    <p>If this member is active in your committee, please go to your committee's
    page in the <a href="{PORTAL_URL}">DSA SF Membership Portal</a> and mark them as active.</p>
    <p>If this is an incorrect request, you can ignore this or
    contact the member at {member_email}.</p>
    <p>If you have any questions, please email
    <a href="mailto:tech@dsasf.org">tech@dsasf.org</a></p>
    <p>Solidarity,</p>
    <p>DSA SF Tech</p>
    </body>
    </html>"""
    recipient_variables = {email: {"name": name}}
    email_connector.send_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject='A member is requesting membership in your committee',
        email_template=template,
        recipient_variables=recipient_variables,
        to_emails=[email],
    )


def send_proxy_nomination_action(
    email_connector: EmailConnector,
    meeting: Meeting,
    nominating_member: Member,
    receiving_member: Member,
    proxy_token: ProxyToken,
):
    sender_name = 'DSA SF Tech Committee'
    sender_email = SUPPORT_EMAIL_ADDRESS

    verb = proxy_token.state
    if verb == ProxyTokenState.ACCEPTED:
        filename = "templates/proxy_nomination_accepted.html"
    else:
        filename = 'templates/proxy_nomination_rejected.html'
    template = (
        pkg_resources.resource_string(membership.__name__, filename)
        .decode("utf-8")
        .format(PORTAL_URL=PORTAL_URL)
    )

    recipient_variables = {
        nominating_member.email_address: {
            "name": nominating_member.first_name,
            "actor_name": receiving_member.first_name,
            "meeting_name": meeting.name,
            "verb": verb.friendly_name,
            "token_link": proxy_token.url,
        },
    }
    subject = (
        "%recipient.actor_name% has %recipient.verb%"
        " your proxy nomination for %recipient.meeting_name%"
    )
    email_connector.send_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject=subject,
        email_template=template,
        recipient_variables=recipient_variables,
        to_emails=[nominating_member.email_address],
    )


def send_meeting_invitation_emails(
    email_connector: EmailConnector,
    invitations: List[MeetingInvitation],
) -> None:
    sender_name = 'DSA SF Tech Committee'
    sender_email = SUPPORT_EMAIL_ADDRESS
    filename = 'templates/meeting_invitation.html'
    template = (
        pkg_resources.resource_string(membership.__name__, filename)
        .decode("utf-8")
        .format(PORTAL_URL=PORTAL_URL)
    )
    recipient_variables = {}
    email_addresses = []
    for invitation in invitations:
        recipient_variables[invitation.member.email_address] = {
            "name": invitation.member.name,
            "meeting_name": invitation.meeting.name,
            "meeting_time": invitation.meeting.start_time.astimezone(
                tz=pytz.timezone('US/Pacific')
            ).strftime('%A, %B %d, %Y at %I:%M %p %Z'),
            "rsvp_url": invitation.rsvp_url,
        }
        email_addresses.append(invitation.member.email_address)
    email_connector.send_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject="You've been invited to %recipient.meeting_name%",
        email_template=template,
        recipient_variables=recipient_variables,
        to_emails=email_addresses,
    )


EMAIL_GROUP_REGEX = r"([^<>]*?) <([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})>"


def parse_combined_sender(sender: str) -> Tuple[Optional[str], Optional[str]]:
    matched = re.match(EMAIL_GROUP_REGEX, sender)
    if matched is None:
        return None, None

    groups = matched.groups()
    if len(groups) < 2:
        return None, None

    return groups[0], groups[1]
