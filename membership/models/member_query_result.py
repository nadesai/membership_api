from membership.models import MemberAsEligibleToVote
from typing import List


class MemberQueryResult:

    def __init__(self,
                 members: List[MemberAsEligibleToVote],
                 cursor: str,
                 has_more: bool):
        self.members = members
        self.cursor = cursor
        self.has_more = has_more
