import enum
from base64 import b64encode
from datetime import datetime, timezone
from typing import List, Optional

from Cryptodome.Random import get_random_bytes
from sqlalchemy import \
    Column, ForeignKey, Integer, BigInteger, Index, Text
from sqlalchemy import and_, func, Enum
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from sqlalchemy.schema import UniqueConstraint

from config import PORTAL_URL
from membership.database.base import Base
from membership.database.types import (
    COMPACT_MAX_LENGTH, StdBoolean, StdString, StdText, UTCDateTime,
    URLSafeToken)
from membership.util.schema import unmodifiable


class Chapter(Base):
    __tablename__ = 'chapters'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), unique=True, nullable=False)


class NationalMembershipData(Base):
    __tablename__ = 'memberships_usa'

    id: int = Column(Integer, primary_key=True, unique=True)
    date_imported: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after we run the script again on prod
        default=func.now(),
    )

    active: bool = Column(StdBoolean(), default=True, nullable=False)
    """
    False if membership has been deactivated.
    Note: Members can deactivate their account before their membership expires.
    """

    member_id: Optional[int] = Column(ForeignKey('members.id'))
    member: Optional['Member'] = relationship(
        'Member',
        back_populates='memberships_usa',
        uselist=False
    )

    ak_id: str = Column(StdString(COMPACT_MAX_LENGTH), unique=True, nullable=False)
    dsa_id: Optional[str] = Column(StdString(COMPACT_MAX_LENGTH), nullable=True)

    do_not_call: bool = Column(StdBoolean(), default=False, nullable=False)

    first_name: str = Column(StdString(), nullable=False)
    middle_name: Optional[str] = Column(StdString())
    last_name: str = Column(StdString(), nullable=False)

    address_line_1: Optional[str] = Column(StdString())
    address_line_2: Optional[str] = Column(StdString())
    city: str = Column(StdString(), nullable=False)  # Optional?
    country: str = Column(StdString(), nullable=False)  # Optional?
    zipcode: str = Column(StdString(10), nullable=False)  # Optional?

    @property
    def address(self) -> List[str]:
        return [
            line for line in (
                self.address_line_1,
                self.address_line_2,
                f"{self.city} {self.zipcode}"
            ) if line
        ]

    # The following should be required, but national sends us an empty value here
    join_date = Column(UTCDateTime)  # Required?
    dues_paid_until = Column(UTCDateTime)  # Required?

    def __repr__(self) -> str:
        return (f"<Membership AK:{self.ak_id} " + f"Name:{self.first_name} {self.last_name} " +
                f"Exp:{self.dues_paid_until}>")


class Member(Base):
    __tablename__ = 'members'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        email = self.email_address
        self.normalized_email = Member.normalize_email(email) if email else None

    id: int = Column(Integer, primary_key=True, unique=True)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after we migrate everyone over
        default=func.now(),
    )

    # guest info for non-members & preferred member info for members
    # todo: consolidate first_name + last_name => nickname & full_name
    first_name: Optional[str] = Column(StdString())
    last_name: Optional[str] = Column(StdString())

    # slash-delimited pronoun declensions. multiple unknown pronoun sets joined by ";"
    pronouns: Optional[str] = Column(StdText())
    email_address: Optional[str] = Column(StdString(), unique=True)
    normalized_email: Optional[str] = Column(StdString(), unique=True)

    # preferences
    do_not_call: bool = Column(StdBoolean(), default=False, nullable=False)
    do_not_email: bool = Column(StdBoolean(), default=False, nullable=False)

    # details
    biography: Optional[str] = Column(StdText())
    phone_numbers: List['PhoneNumber'] = relationship('PhoneNumber', back_populates='member')
    additional_email_addresses: List['AdditionalEmailAddress'] = relationship(
        'AdditionalEmailAddress',
        back_populates='member'
    )
    address: Optional[str] = Column(StdString())
    city: Optional[str] = Column(StdString())
    zipcode: Optional[str] = Column(StdString())

    # admin fields
    notes = Column(StdText(), default="", nullable=False)
    memberships_usa: List['NationalMembershipData'] = relationship(
        'NationalMembershipData',
        back_populates='member',
    )
    identities: List['Identity'] = relationship('Identity', back_populates='member')
    eligible_votes: List['EligibleVoter'] = relationship('EligibleVoter', back_populates='member')
    meetings_attended: List['Attendee'] = relationship('Attendee', back_populates='member')
    meetings_owned: List['Meeting'] = relationship('Meeting', back_populates='owner')
    roles: List['Role'] = relationship('Role', back_populates='member')
    email_verify_token: List['AttendeeEmailVerifyToken'] = relationship(
        'AttendeeEmailVerifyToken',
        back_populates='member',
        cascade='all, delete-orphan'
    )
    meeting_invitations: List['MeetingInvitation'] = relationship(
        'MeetingInvitation',
        back_populates='member',
    )

    @property
    def name(self) -> str:
        n = ''
        if self.first_name:
            n = self.first_name
        if self.last_name:
            n += ' ' + self.last_name
        return n

    @staticmethod
    def normalize_email(email: str) -> str:
        """WARNING: this method removes `.` characters from the local part of email addresses.
        As far as I (derrickliu) can tell, this is behavior specific to consumer Gmail
        and doesn't actually apply to other email providers"""
        lowercased = email.lower()
        splitted = lowercased.split('@')
        if len(splitted) != 2:
            raise ValueError('Invalid email address: "{}"'.format(email))

        local, host = splitted
        if host == "gmail.com" or host == "googlemail.com":
            # realistically we can only assume this for consumer gmail
            cleaned = local.replace('.', '')
        else:
            cleaned = local

        return ''.join([cleaned, '@', host])


class PhoneNumber(Base):
    __tablename__ = 'phone_numbers'
    __table_args__ = (
        UniqueConstraint('member_id', 'number'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    date_imported: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after we run the script again on prod
        default=func.now(),
    )

    number: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False)
    name: Optional[str] = Column(StdString())

    member: Member = relationship('Member', back_populates='phone_numbers')


class AdditionalEmailAddress(Base):
    __tablename__ = 'additional_email_addresses'
    __table_args__ = (
        UniqueConstraint('member_id', 'email_address'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )
    date_verified: datetime = Column(UTCDateTime, nullable=True)

    email_address: str = Column(StdString(), nullable=False)
    name: Optional[str] = Column(StdString())
    preferred: bool = Column(StdBoolean(), default=True, nullable=False)
    verified: bool = Column(StdBoolean(), default=False, nullable=False)

    member: Member = relationship('Member', back_populates='additional_email_addresses')


class MembershipSources:
    national: str = 'USA'
    san_francisco: str = 'SF'


class IdentityProviders:
    AK = 'AK_ID'
    DSA = 'DSA_ID'
    AUTH0 = 'AUTH0'
    COLLECTIVE_DUES = 'COLLECTIVE_DUES_SUBSCRIPTION'
    CSV = 'CSV'


class Identity(Base):
    __tablename__ = 'identities'
    __table_args__ = (
        UniqueConstraint('provider_id', 'provider_name'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    date_imported: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after we run the script again on prod
        default=func.now(),
    )

    provider_name: str = Column(StdString(), nullable=False)  # one of IdentityProviders
    provider_id: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False)

    member: 'Member' = relationship('Member', back_populates='identities')


class Committee(Base):
    __tablename__ = 'committees'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(COMPACT_MAX_LENGTH))
    provisional: bool = Column(StdBoolean(), default=False, nullable=False)

    emails: List['Email'] = relationship('Email', back_populates='committee')
    admins: List['Member'] = relationship('Member', secondary='roles',
                                          primaryjoin="and_(Committee.id==Role.committee_id, "
                                          "Role.role=='admin')")
    members: List['Member'] = relationship('Member', secondary='roles',
                                           primaryjoin="and_(Committee.id==Role.committee_id, "
                                           "Role.role=='member')")
    active: List['Member'] = relationship('Member', secondary='roles',
                                          primaryjoin="and_(Committee.id==Role.committee_id, "
                                          "Role.role=='active')")


class Role(Base):
    __tablename__ = 'roles'
    __table_args__ = (UniqueConstraint('member_id', 'committee_id', 'role'), )

    id: int = Column(Integer, primary_key=True, unique=True)
    chapter_id: Optional[int] = Column(ForeignKey('chapters.id'))
    committee_id: Optional[int] = Column(ForeignKey('committees.id'))
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    role: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=True,  # TODO: Remove after roles are migrated on prod
        default=func.now()
    )

    committee: 'Committee' = relationship(Committee)
    member: 'Member' = relationship('Member', back_populates='roles')


class Meeting(Base):
    __tablename__ = 'meetings'
    __table_args__ = (UniqueConstraint('name'),)

    id: int = Column(Integer, primary_key=True, unique=True)
    short_id: Optional[int] = Column(Integer, nullable=True, unique=True)
    name: str = Column(StdString(), nullable=False)
    committee_id: Optional[int] = Column(ForeignKey('committees.id'))
    start_time: Optional[datetime] = Column(UTCDateTime)
    end_time: Optional[datetime] = Column(UTCDateTime)
    landing_url: Optional[str] = Column(StdString(), nullable=True)
    owner_id: int = Column(ForeignKey('members.id'))
    published: bool = Column(StdBoolean(), default=True, nullable=False)

    owner: 'Member' = relationship('Member', back_populates='meetings_owned')
    attendees: List['Attendee'] = relationship('Attendee', back_populates='meeting')
    invitations: List['MeetingInvitation'] = relationship(
        'MeetingInvitation',
        back_populates='meeting',
    )
    agenda: 'MeetingAgenda' = relationship('MeetingAgenda', uselist=False, back_populates='meeting')

    @hybrid_property
    def is_general_meeting(self):
        return (self.committee_id == None) and ('General' in self.name)

    @is_general_meeting.expression
    def is_general_meeting(cls):
        return and_(cls.committee_id == None, cls.name.like('%General%'))


class Attendee(Base):
    __tablename__ = 'attendees'

    __table_args__ = (
        Index('attendee_member_id_meeting_id', 'member_id', 'meeting_id', unique=True),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    meeting_id: int = Column(ForeignKey('meetings.id'))
    member_id: int = Column(ForeignKey('members.id'))
    eligible_to_vote: Optional[bool] = Column(StdBoolean())
    verified: bool = Column(StdBoolean(), nullable=False, default=True)

    member: 'Member' = relationship('Member', back_populates='meetings_attended')
    meeting: 'Meeting' = relationship('Meeting', back_populates='attendees')
    email_verify_token: 'AttendeeEmailVerifyToken' = relationship(
        'AttendeeEmailVerifyToken',
        back_populates='attendee',
        cascade='all, delete-orphan',
        uselist=False
    )


class AttendeeEmailVerifyToken(Base):
    __tablename__ = 'attendee_email_verify_tokens'

    __table_args__ = (
        UniqueConstraint('attendee_id', 'email_address', 'token'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    created_on: datetime = Column(UTCDateTime, default=func.now())
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    attendee_id: int = Column(ForeignKey('attendees.id'), nullable=False)
    email_address: str = Column(StdString(), unique=True)
    token: str = Column(StdString(), unique=True)

    member: 'Member' = relationship(
        'Member',
        back_populates='email_verify_token',
        uselist=False
    )
    attendee: 'Attendee' = relationship(
        'Attendee',
        back_populates='email_verify_token',
        uselist=False
    )


class MeetingInvitationStatus(enum.Enum):
    SEND_PENDING = 'SEND_PENDING'
    NO_RESPONSE = 'NO_RESPONSE'
    ACCEPTED = 'ACCEPTED'
    DECLINED = 'DECLINED'


class MeetingInvitation(Base):
    __tablename__ = 'meeting_invitation'

    __table_args__ = (
        UniqueConstraint('meeting_id', 'member_id'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    created_at: datetime = Column(UTCDateTime, default=func.now())
    meeting_id: int = Column(ForeignKey('meetings.id'), nullable=False)
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    token: str = unmodifiable(Column(
        StdString(128),
        unique=True,
        default=lambda x: URLSafeToken.column_default(),
    ))
    status: MeetingInvitationStatus = Column(
        Enum(MeetingInvitationStatus),
        default=MeetingInvitationStatus.SEND_PENDING,
    )

    member: 'Member' = relationship('Member', back_populates='meeting_invitations')
    meeting: 'Meeting' = relationship('Meeting', back_populates='invitations')

    @property
    def rsvp_url(self):
        url_encoded_token = URLSafeToken.url_encoded_token(self.token)
        return f'{PORTAL_URL}/meetings/{self.meeting_id}/rsvp/{url_encoded_token}'


class MeetingAgenda(Base):
    __tablename__ = 'meeting_agendas'

    id: int = Column(Integer, primary_key=True, unique=True)
    meeting_id: int = Column(ForeignKey('meetings.id'), nullable=False, unique=True)
    text: str = Column(StdText(), default='', nullable=False)
    updated_at: datetime = Column(UTCDateTime, default=func.now(), nullable=False)

    meeting: 'Meeting' = relationship('Meeting', back_populates='agenda', uselist=False)


class Election(Base):
    __tablename__ = 'elections'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), nullable=False, unique=True)
    status: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False, default='draft')
    number_winners: int = Column(Integer)
    voting_begins_epoch_millis: Optional[int] = Column(BigInteger, nullable=True)
    voting_ends_epoch_millis: Optional[int] = Column(BigInteger, nullable=True)
    description: Optional[str] = Column(StdText(), nullable=True)
    description_img: Optional[str] = Column(StdString(1024), nullable=True)
    author: Optional[str] = Column(StdString(), nullable=True)

    candidates: List['Candidate'] = relationship('Candidate', back_populates='election')
    sponsors: List['Sponsor'] = relationship('Sponsor', back_populates='election')
    votes: List['Vote'] = relationship('Vote', back_populates='election')
    voters: List['EligibleVoter'] = relationship('EligibleVoter', back_populates='election')

    @property
    def voting_begins(self) -> Optional[datetime]:
        if self.voting_begins_epoch_millis is None:
            return None
        return (
            datetime.utcfromtimestamp(self.voting_begins_epoch_millis / 1000.0)
            .replace(tzinfo=timezone.utc)
        )

    @property
    def voting_ends(self) -> Optional[datetime]:
        if self.voting_ends_epoch_millis is None:
            return None
        return (
            datetime.utcfromtimestamp(self.voting_ends_epoch_millis / 1000.0)
            .replace(tzinfo=timezone.utc)
        )


class Candidate(Base):
    __tablename__ = 'candidates'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString())
    election_id: int = Column(ForeignKey('elections.id'))
    image_url: Optional[str] = Column(StdString(), nullable=True)

    election: 'Election' = relationship('Election', back_populates='candidates')


class Sponsor(Base):
    __tablename__ = 'sponsors'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), nullable=False)
    election_id: int = Column(ForeignKey('elections.id'), nullable=False)

    election: 'Election' = relationship('Election', back_populates='sponsors')


class Vote(Base):
    __tablename__ = 'votes'
    __table_args__ = (UniqueConstraint('vote_key', 'election_id'), )

    id: int = Column(Integer, primary_key=True, unique=True)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after votes are migrated on prod
        default=func.now(),
    )
    vote_key: int = Column(Integer)
    election_id: int = Column(ForeignKey('elections.id'))

    election: 'Election' = relationship('Election', back_populates='votes')
    ranking: List['Ranking'] = relationship(
        'Ranking', back_populates='vote', order_by='Ranking.rank')


class Ranking(Base):
    __tablename__ = 'rankings'

    id: int = Column(Integer, primary_key=True, unique=True)
    vote_id: int = Column(ForeignKey('votes.id'))
    rank: int = Column(Integer)
    candidate_id: int = Column(ForeignKey('candidates.id'))

    vote: 'Vote' = relationship('Vote', back_populates='ranking')
    candidate: 'Candidate' = relationship('Candidate')


class EligibleVoter(Base):
    __tablename__ = 'eligible_voters'

    __table_args__ = (
        Index('eligible_voter_member_election_unique', 'member_id', 'election_id', unique=True),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey('members.id'))
    voted: bool = Column(StdBoolean(), nullable=False)
    election_id: int = Column(ForeignKey('elections.id'))

    member: 'Member' = relationship('Member', back_populates='eligible_votes')
    election: 'Election' = relationship('Election', back_populates='voters')


class Email(Base):
    """
    An incoming email address for the chapter, which will forward to some external email addresses.
    """
    __tablename__ = 'emails'

    id: int = Column(Integer, primary_key=True, unique=True)
    # an external id pointing to the configuration in the external mail routing provider
    external_id: str = Column(StdString(), unique=True)
    email_address: str = Column(StdString(), unique=True, nullable=False)
    committee_id: int = Column(ForeignKey('committees.id'))

    committee: 'Committee' = relationship(Committee, back_populates='emails')
    forwarding_addresses: List['ForwardingAddress'] = relationship('ForwardingAddress',
                                                                   cascade='all, delete-orphan',
                                                                   back_populates='incoming_email')


class InterestTopic(Base):
    __tablename__ = 'interest_topics'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), nullable=False, unique=True)


class EmailTemplate(Base):
    __tablename__ = 'email_templates'

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), nullable=False)
    subject: str = Column(Text)
    topic_id: int = Column(ForeignKey('interest_topics.id'), nullable=True)
    body: str = Column(Text)
    last_updated: datetime = Column(UTCDateTime(), nullable=False, server_default=func.now())

    topic: InterestTopic = relationship(InterestTopic)


class ForwardingAddress(Base):
    __tablename__ = 'forwarding_addresses'

    id: int = Column(Integer, primary_key=True, unique=True)
    forward_to: str = Column(StdString())
    incoming_email_id: int = Column(ForeignKey('emails.id'))

    incoming_email: Email = relationship(Email, back_populates='forwarding_addresses')


class Interest(Base):
    __tablename__ = 'interests'
    __table_args__ = (
        UniqueConstraint('member_id', 'topic_id'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    topic_id: int = Column(ForeignKey('interest_topics.id'), nullable=False)
    created_date: datetime = Column(UTCDateTime, nullable=False, default=func.now())

    member: Member = relationship(Member)
    topic: InterestTopic = relationship(InterestTopic)


class InterestEmailSent(Base):
    __tablename__ = 'interest_emails_sent'

    id: int = Column(Integer, primary_key=True, unique=True)
    interest_id: Column(ForeignKey('interests.id'), nullable=False)
    template_id: Column(ForeignKey('email_templates.id'), nullable=False)
    date_sent: datetime = Column(UTCDateTime, nullable=False, default=func.now())


class AssetType(enum.Enum):
    EXTERNAL_URL = 'EXTERNAL_URL'
    PRESIGNED_URL = 'PRESIGNED_URL'


class AssetLabel(Base):
    __tablename__ = 'asset_labels'
    __table_args__ = (
        UniqueConstraint('asset_id', 'label'),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    asset_id: int = Column(ForeignKey('assets.id', ondelete='CASCADE'), nullable=False)
    label: str = Column(StdString(), nullable=False)

    asset: 'Asset' = relationship('Asset', back_populates='labels')


class Asset(Base):
    __tablename__ = 'assets'

    id: int = Column(Integer, primary_key=True, unique=True)
    asset_type: AssetType = Column(Enum(AssetType), nullable=False)
    last_updated: datetime = Column(UTCDateTime, nullable=False, default=func.now())
    content_type: str = Column(StdString(), nullable=False)
    title: Optional[str] = Column(StdString())
    external_url: Optional[str] = Column(StdText())
    relative_path: Optional[str] = Column(StdString(), unique=True)

    labels: List[AssetLabel] = relationship(AssetLabel, back_populates='asset')


class AuthToken(Base):
    """Encapsulates tokens used to identify users without Auth0 authorization."""

    __tablename__ = 'auth_token'

    # A unique identifier for this token, which is used as effective authorization of this user.
    #
    # THIS IS PRIVATE INFORMATION.
    id: str = unmodifiable(Column(
        StdString(128),
        primary_key=True,
        unique=True,
        default=str(b64encode(get_random_bytes(32))),
    ))

    # ID of the calling user.
    member_id: int = unmodifiable(Column(Integer, nullable=False))

    # Route to allow on requests tagged with this token.
    path: str = unmodifiable(Column(StdString(), nullable=False))

    # JSON Parameter field mask to enforce on requests using this token.
    # Must be a valid JSON string.
    request_json_params: str = unmodifiable(Column(StdString(), nullable=True))

    # If set, this is a single-use token, and will be invalidated after one
    # request.
    single_use: bool = unmodifiable(Column(StdBoolean(), default=False, nullable=False))

    # If set, we will not honor this token after the given date.
    expire_date: datetime = unmodifiable(Column(UTCDateTime, nullable=True))

    # If set with single_use, we will not honor this token.
    used: bool = Column(StdBoolean(), default=False, nullable=False)


class ProxyTokenState(enum.Enum):
    UNCLAIMED = 'UNCLAIMED'
    ACCEPTED = 'ACCEPTED'
    REJECTED = 'REJECTED'

    @property
    def friendly_name(self) -> str:
        return self.value.lower()


class ProxyToken(Base):
    """
    Tokens that members can create
    to nominate other members to vote on their behalf at a meeting.
    """

    __tablename__ = 'proxy_token'

    # A unique identifier for this token, which is used as effective authorization of this user.
    id: str = unmodifiable(Column(
        StdString(128),
        primary_key=True,
        unique=True,
        default=lambda x: URLSafeToken.column_default(),
    ))

    # ID of the member who nominating someone else to be their proxy.
    member_id: int = unmodifiable(Column(Integer, nullable=False))

    # ID of the meeting at which the Proxy will vote.
    meeting_id: int = unmodifiable(Column(Integer, nullable=False))

    # ID of the member who is being nominated.
    receiving_member_id: int = Column(Integer, nullable=True)

    # Tokens can only be used once.
    state: ProxyTokenState = Column(
        Enum(ProxyTokenState),
        nullable=False,
        default=ProxyTokenState.UNCLAIMED,
    )

    @property
    def url(self):
        url_encoded_token = URLSafeToken.url_encoded_token(self.id)
        return f'{PORTAL_URL}/meetings/{self.meeting_id}/proxy-token/{url_encoded_token}'
