"""add member admin notes column

Revision ID: 2843b8c6db14
Revises: 5f3cdf445fff
Create Date: 2020-02-06 02:49:35.778196

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '2843b8c6db14'
down_revision = '1d284c694423'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('members', sa.Column('notes', sa.Text(), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('members', 'notes')
    # ### end Alembic commands ###
