from datetime import datetime

import pytest

import membership.services
from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import Email, Member, Role, AdditionalEmailAddress
from membership.models import AuthContext
from membership.services import (
    EmailExistsError,
    EmailService,
    AdditionalEmailAddressService,
)
from membership.util.email import EmailConnector

email_service = EmailService()
# Monkey patch the email_connector
membership.services.email_service.email_connector = EmailConnector()


class TestEmailService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()

        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role1 = Role(member=m, role="member", date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role="admin", date_created=datetime(2016, 1, 1))
        email = Email(external_id=1, email_address=SUPER_USER_EMAIL)
        session.add_all([role1, role2, email])

        session.commit()

        self.session = session
        self.member = m
        self.ctx = AuthContext(self.member, self.session)

    def teardown(self):
        metadata.drop_all(engine)


class TestEmailServiceList(TestEmailService):
    def test_list_returns_email_info(self):
        emails = email_service.list(self.ctx)

        assert len(emails) == 1

        email = emails[0]

        assert email.id == 1
        assert email.email_address == SUPER_USER_EMAIL
        assert email.forwarding_addresses == []


class TestEmailServiceCreate(TestEmailService):
    def test_create_creates_a_new_email(self):
        email_address = "alice@example.com"
        forwarding_address = "bob@example.com"
        email = email_service.create(self.ctx, email_address, [forwarding_address],)

        assert email.email_address == email_address
        assert email.forwarding_addresses[0].forward_to == forwarding_address

        email = (
            self.session.query(Email)
            .filter_by(email_address=email_address)
            .one_or_none()
        )

        assert email is not None

    def test_create_raises_an_exception_for_existing_emails(self):
        email_address = SUPER_USER_EMAIL

        with pytest.raises(EmailExistsError):
            email_service.create(self.ctx, email_address=email_address)


class TestEmailServiceGet(TestEmailService):
    def test_get_finds_existing_email(self):
        assert email_service.get(self.ctx, 1) is not None

    def test_get_returns_none_for_missing_email(self):
        assert email_service.get(self.ctx, -1) is None


class TestEmailServiceUpdate(TestEmailService):
    def test_update_changes_the_email_and_forwarding_addresses(self):
        alice = "alice@example.com"
        bob = "bob@example.com"
        carol = "carol@example.com"
        email = email_service.create(
            self.ctx, email_address=alice, forwarding_addresses=[alice, bob],
        )
        email = email_service.update(
            self.ctx,
            email_id=email.id,
            email_address=bob,
            forwarding_addresses=[bob, carol],
        )
        forwarding_addresses = [f.forward_to for f in email.forwarding_addresses]
        assert email.email_address == bob
        assert len(email.forwarding_addresses) == 2
        assert bob in forwarding_addresses
        assert carol in forwarding_addresses

    def test_update_returns_none_for_missing_email(self):
        assert email_service.update(self.ctx, -1) is None


class TestEmailServiceDelete(TestEmailService):
    def test_deletes_existing_email(self):
        assert email_service.delete(self.ctx, 1)
        assert (
            self.session.query(Email)
            .filter_by(email_address=SUPER_USER_EMAIL,)
            .one_or_none()
            is None
        )

    def test_returns_false_for_missing_email(self):
        assert not email_service.delete(self.ctx, -1)


class TestAdditionalEmailService:
    def setup(self):
        metadata.create_all(engine)
        session = Session()
        m = Member(id=1)
        m.email_address = "someone@example.com"
        m.notes = "Some notes on this member"
        session.add(m)
        role1 = Role(member=m, role="member", date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role="admin", date_created=datetime(2016, 1, 1))
        session.add_all([role1, role2])

    def teardown(self):
        metadata.drop_all(engine)

    def test_add_addl_email(self):
        session = Session()
        addl_email_address = AdditionalEmailAddress(
            member_id=1, name="Example", email_address="additional@example.com"
        )
        pnservice = AdditionalEmailAddressService()
        pnservice.add_all(session, [addl_email_address])
        result = session.query(AdditionalEmailAddress).first()
        assert result is not None
        assert result.name == "Example"
        assert result.email_address == "additional@example.com"
