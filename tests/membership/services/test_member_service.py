import pytest

from membership.database.base import engine, metadata, Session
from membership.database.models import Member
from membership.models import MemberAsEligibleToVote
from membership.services import MemberService
from membership.services.eligibility import EligibilityService

from overrides import overrides
from typing import List

test_emails = [
    ("leon.trotsky24@gmail.com", "leontrotsky24@gmail.com"),
    ("gerry.cohen....@gmail.com", "gerrycohen@gmail.com"),
    ("test.e.m.a.i.l.@googlemail.com", "testemail@googlemail.com"),
    ("nikita..kr00schev@ussr.cool.com", "nikita..kr00schev@ussr.cool.com"),
    ("test.e.m.a.i.l@outlook.com", "test.e.m.a.i.l@outlook.com")
]


class FakeEligibilityService(EligibilityService):
    def __init__(self) -> None:
        pass

    @overrides
    def members_as_eligible_to_vote(
        self,
        session: Session,
        members: List[Member]
    ) -> List[MemberAsEligibleToVote]:
        return [
            MemberAsEligibleToVote(
                member,
                is_eligible=True,
                message="message"
            ) for member in members
        ]


class TestMemberService:
    def setup(self):
        metadata.create_all(engine)

    def teardown(self):
        metadata.drop_all(engine)

    def test_get_by_email(self):
        session = Session()
        target = test_emails[0][0]
        session.add(Member(email_address=target))
        session.commit()
        ms = MemberService(FakeEligibilityService())
        m = ms.find_by_email(target, session)
        assert m.email_address == target

    def test_normalize_email(self):
        for original, expected in test_emails:
            m = Member(email_address=original)
            assert m.normalized_email == expected

    def test_normalize_email_invalid_email(self):
        with pytest.raises(ValueError):
            Member(email_address='lparsons@invalid@domain.com')

    def populate_with_n_members(self, n):
        session = Session()
        for i in range(0, n):
            m = Member()
            m.first_name = 'First%d' % i
            m.last_name = 'Last%d' % i
            m.email_address = 'user%d@foo.com' % i
            session.add(m)
        session.commit()

    def test_query_all(self):
        num_members = 10
        self.populate_with_n_members(num_members)
        session = Session()
        ms = MemberService(FakeEligibilityService())
        member_query_result = ms.all(session)

        assert len(member_query_result.members) == num_members

    def test_query_all_no_members(self):
        session = Session()
        ms = MemberService(FakeEligibilityService())
        member_query_result_1 = ms.all(session)

        assert len(member_query_result_1.members) == 0

        # Now populate with some members
        self.populate_with_n_members(3)

        # Note, even though the previous query didn't return anything, passing
        # in the returned cursor to a subsequent query is valid (and will
        # return all new members)
        member_query_result_2 = ms.query(cursor=member_query_result_1.cursor,
                                         page_size=10,
                                         query_str=None,
                                         session=session)

        assert len(member_query_result_2.members) == 3
        assert not member_query_result_2.has_more

    def test_query_paginated(self):
        session = Session()
        page_size = 5
        self.populate_with_n_members(12)
        ms = MemberService(FakeEligibilityService())

        member_query_result_1 = ms.query(cursor=None, page_size=page_size,
                                         query_str=None, session=session)
        member_query_result_2 = ms.query(cursor=member_query_result_1.cursor, page_size=page_size,
                                         query_str=None, session=session)
        member_query_result_3 = ms.query(cursor=member_query_result_2.cursor, page_size=page_size,
                                         query_str=None, session=session)

        assert len(member_query_result_1.members) == 5
        assert len(member_query_result_2.members) == 5
        assert len(member_query_result_3.members) == 2

        assert member_query_result_1.has_more
        assert member_query_result_2.has_more
        assert not member_query_result_3.has_more

    def test_query_with_search_filter(self):
        session = Session()
        num_members = page_size = 12
        self.populate_with_n_members(num_members)

        ms = MemberService(FakeEligibilityService())
        first_name_query_result = ms.query(cursor=None, page_size=page_size,
                                           query_str='First', session=session)
        last_name_query_result = ms.query(cursor=None, page_size=page_size,
                                          query_str='Last', session=session)
        email_prefix_query_result = ms.query(cursor=None, page_size=page_size,
                                             query_str='user1', session=session)
        email_suffix_query_result = ms.query(cursor=None, page_size=page_size,
                                             query_str='foo.com', session=session)

        # Should return users with names 'First1', 'First10' and 'First11'
        first_name_query_result_2 = ms.query(cursor=None, page_size=page_size,
                                             query_str='First1', session=session)

        assert len(first_name_query_result.members) == num_members
        assert len(last_name_query_result.members) == num_members
        assert len(email_prefix_query_result.members) == 3
        assert len(email_suffix_query_result.members) == 0
        assert len(first_name_query_result_2.members) == 3

    def test_query_with_search_filter_returns_no_results(self):
        session = Session()
        num_members = page_size = 10
        self.populate_with_n_members(num_members)

        ms = MemberService(FakeEligibilityService())
        first_name_query_result = ms.query(cursor=None, page_size=page_size,
                                           query_str='ASDF', session=session)

        assert len(first_name_query_result.members) == 0

    def test_paginate_with_search_filter(self):
        session = Session()
        num_members = 12
        self.populate_with_n_members(num_members)

        ms = MemberService(FakeEligibilityService())

        # Overall search should return users with names 'First1', 'First10' and 'First11'
        first_name_query_result_1 = ms.query(cursor=None, page_size=2,
                                             query_str='First1', session=session)
        first_name_query_result_2 = ms.query(cursor=first_name_query_result_1.cursor, page_size=2,
                                             query_str='First1', session=session)

        assert len(first_name_query_result_1.members) == 2
        assert len(first_name_query_result_2.members) == 1

        assert first_name_query_result_1.has_more
        assert not first_name_query_result_2.has_more
