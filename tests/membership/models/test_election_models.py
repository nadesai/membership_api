from membership.models import ElectionStateMachine


class TestElectionStateMachine:

    def test_default_status(self):
        result = ElectionStateMachine().status
        expected = 'draft'

        assert result == expected

    def test_transition_from_draft(self):
        election = ElectionStateMachine()
        election.publish()
        result = election.status
        expected = 'published'

        assert result == expected

    def test_transition_from_published(self):
        election = ElectionStateMachine('published')
        election.finalize()
        result = election.status
        expected = 'final'

        assert result == expected

    def test_next_transitions_draft(self):
        election = ElectionStateMachine('draft')
        result = election.next_transitions()
        expected = ['publish', 'cancel']

        assert result == expected

    def test_next_transitions_published(self):
        election = ElectionStateMachine('published')
        result = election.next_transitions()
        expected = ['finalize', 'cancel']

        assert result == expected

    def test_next_transitions_final(self):
        election = ElectionStateMachine('final')
        result = election.next_transitions()
        expected = []

        assert result == expected
