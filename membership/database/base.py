from typing import Any, cast
from typing import Callable, Union
from typing import Type

import sqlalchemy.dialects.mysql
from sqlalchemy import create_engine, event, orm, Column
from sqlalchemy.exc import DisconnectionError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.selectable import TableClause

from config import settings


def checkout_listener(dbapi_con, con_record, con_proxy):
    """
    Ensures that connections in the pool are still valid before returning them
    :param dbapi_con:
    :param con_record:
    :param con_proxy:
    :return:
    """
    try:
        try:
            dbapi_con.ping(False)
        except TypeError:
            dbapi_con.ping()
    except dbapi_con.OperationalError as exc:
        if exc.args[0] in (2006, 2013, 2014, 2045, 2055):
            raise DisconnectionError()
        else:
            raise


Base = declarative_base()
metadata: sqlalchemy.schema.MetaData = Base.metadata
engine: sqlalchemy.engine.Engine = create_engine(**settings)
Session: Union[Callable[[], orm.Session], orm.Session] = orm.sessionmaker(bind=engine)
event.listen(engine, 'checkout', checkout_listener)


def col(field: Any) -> Column:
    return cast(Column, field)


def table(base: Type[Base]) -> TableClause:
    return cast(TableClause, base)
