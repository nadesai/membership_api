from typing import List

from sqlalchemy import insert

from membership.database.base import Session
from membership.database.models import Identity


class IdentityService:
    def add_all(
            self,
            session: Session,
            identities: List[Identity],
    ) -> None:
        for identity in identities:
            insert(Identity).prefix_with("IGNORE").values(
                member_id=identity.member_id,
                provider_name=identity.provider_name,
                provider_id=identity.provider_id,
            )
        session.commit()
